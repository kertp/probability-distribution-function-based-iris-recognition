#include <fts.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "reader.hpp"
#include "regexer.hpp"

using namespace std;

vector<string> Reader::read( string path ){
  vector<string> fileList;
  try{
    // string to char*
    char* cstr = strdup( path.c_str() );

    char *dot[] = {cstr, 0};
    char **paths = dot;


    FTS *tree = fts_open(paths, FTS_NOCHDIR, 0);
    if (!tree) {
      throw("Error opening");
    }

    FTSENT *node;
    while ((node = fts_read(tree))) {
      if (node->fts_level > 0 && node->fts_name[0] == '.'){
        fts_set(tree, node, FTS_SKIP);
      }
      else if (node->fts_info == FTS_F) {
        fileList.push_back( node->fts_path );
      }
    }

    fts_close(tree);
    free(cstr);
    
    return fileList;
  }
  catch( const char* msg ){
    cout << msg << endl;
    return fileList;
  }
}

vector<string> Reader::match( string path, string regexpression ){
  Regexer regexer;
  vector<string> result = this->read( path );
  vector<string> final;
  for( int i = 0; i < result.size(); i++ ){
    if(regexer.find(result[i], regexpression))
      final.push_back(result[i]);
  }
  return final;
}