#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include "image_handeler.hpp"
#include <math.h>

// Exceptions
#include <stdexcept>

// IO
#include <iostream>
#include <fstream>

// min
#include <algorithm>

using namespace cv;
using namespace std;

struct range ImageHandeler::get_row_range( Mat first_im, Mat second_im ){

  struct range row;
  int row_diff = abs(first_im.rows - second_im.rows);
  int row_start = row_diff / 2;
  int row_end = row_start + second_im.rows;

  row.start = row_start;
  row.end = row_end;

  return row;
}

struct range ImageHandeler::get_col_range( Mat first_im, Mat second_im ){

  struct range col;
  int col_diff = abs(first_im.cols - second_im.cols);
  int col_start = col_diff / 2;
  int col_end = col_start + second_im.cols;

  col.start = col_start;
  col.end = col_end;

  return col;
}

int ImageHandeler::save_img( Mat img, string filename ){
  imwrite( filename, img );
  cout << "Saved an image: " << filename << endl;
}

int ImageHandeler::filter( string file, string mask_file ){
  Mat mask = imread( mask_file, CV_LOAD_IMAGE_GRAYSCALE );
  Mat original_iris = imread(file);
  if (original_iris.empty())
  {
    cout << "Error : Image cannot be loaded..!!" << endl;
    return -1;
  }
  if (mask.empty())
  {
    cout << "Error : Mask cannot be loaded..!!" << endl;
    return -1;
  }

  struct range row = this->get_row_range(original_iris, mask);
  struct range col = this->get_col_range(original_iris, mask);
  Mat iris = original_iris.rowRange(row.start,row.end).colRange(col.start,col.end);

  for(int i = 0; i < iris.rows; i++){
    for(int j = 0; j < iris.cols; j++){
      int mask_bit = mask.at<uchar>(j,i);
      if(0 == mask_bit){
        iris.at<Vec3b>(j,i)[0] = 0;
        iris.at<Vec3b>(j,i)[1] = 0;
        iris.at<Vec3b>(j,i)[2] = 0;
      }
    }
  }
  string save_path = file;
  save_path.erase(8,9);
  save_path.insert(8, "/filtered");
  this->save_img( iris, save_path );
  return 0;
}

vector<Mat> ImageHandeler::splitImg( Mat input ){

  vector<Mat> bgr_planes(3);
  split( input, bgr_planes );
  return bgr_planes;
}

Mat ImageHandeler::bgr2hsi( Mat image ){
  Mat hsi;
  int a;
  hsi.create(image.rows, image.cols, CV_8UC3);
  for(int i = 0; i < image.rows; i++){
    for(int j = 0; j < image.cols; j++){
      double hue, saturation, intensity, minimum, third;
      double b = image.at<Vec3b>(i,j)[0];
      double g = image.at<Vec3b>(i,j)[1];
      double r = image.at<Vec3b>(i,j)[2];

      intensity = (b + g + r) / 3;
      minimum = min( b, min( g, r ) );

      if( intensity == 0 ){
        saturation = 0;
      }
      else{
        saturation = 1 - minimum/intensity;
      }

      double first = sqrt( pow( r, 2 ) + pow( g, 2 ) + pow( b, 2 ) - r*g - r*b - g*b );

      double second = r - 0.5*g - 0.5*b;
      if(first == 0){
        hue = 0;
      }
      else{
        third = second / first;
        double rad = acos( third );
        if(g >= b){
          hue = this->rad2deg( rad );
        }
        else{
          hue = 360 - this->rad2deg( rad ); 
        }
      }

      hsi.at<Vec3b>(i,j)[2] = hue;
      hsi.at<Vec3b>(i,j)[1] = saturation * 255;
      hsi.at<Vec3b>(i,j)[0] = intensity * 255;
    } 
  }
  return hsi;
}

int ImageHandeler::rad2deg( int rad ){
  return (rad * 180) / PI;
}

int ImageHandeler::writeHist( const char *file, Mat histogram){
  ofstream out_file;
  out_file.open(file);
  if(out_file.fail()){
    cout << "Opening file failed" << endl;
    return -1;
  }
  for(int i = 0; i < histogram.rows; i++){
    out_file <<  histogram.at<float>(i) << endl;
  }
  out_file.close();

  return 1;
}

Mat ImageHandeler::genHist( Mat image, int bins ){

  if(image.channels() != 1){
    stringstream sstm;
    sstm << "The image has " << image.channels() << " channels instead of one. Please split it!" << endl;
    string result = sstm.str();
    throw std::invalid_argument( result );
  }

  Mat hist;
  float range[] = { 0, 256 } ;
  const float* histRange = { range };
  // http://docs.opencv.org/modules/imgproc/doc/histograms.html?highlight=calchist#void calcHist(const Mat* images, int nimages, const int* channels, InputArray mask, SparseMat& hist, int dims, const int* histSize, const float** ranges, bool uniform, bool accumulate)
  // http://docs.opencv.org/doc/tutorials/imgproc/histograms/histogram_calculation/histogram_calculation.html
  calcHist( &image, 1, 0, Mat(), hist, 1, &bins, &histRange, UNIFORM, ACCUMULATE );
  return hist;
}


vector<Mat> ImageHandeler::getHist( Mat input ){
  vector<Mat> channels(3);
  channels = this->splitImg( input );

  vector<Mat> histograms(3);
  for(int i = 0; i < 3; i++){
    histograms[i] = this->genHist( channels[i], BINS);
  }
    
  int hist_w = 800; int hist_h = 800;
  int bin_w = cvRound( (double) hist_w/BINS );

  // Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );
  

  histograms[0].at<float>(0) = 0;
  histograms[1].at<float>(0) = 0;
  histograms[2].at<float>(0) = 0;

  // / Draw for each channel
  // for( int i = 1; i < BINS; i++ )
  // {
  //     line( histImage, Point( bin_w*(i-1), hist_h - cvRound((histograms[0]).at<float>(i-1)) ) ,
  //                      Point( bin_w*(i), hist_h - cvRound((histograms[0]).at<float>(i)) ),
  //                      Scalar( 255, 0, 0), 2, 8, 0  );
  //     line( histImage, Point( bin_w*(i-1), hist_h - cvRound(histograms[1].at<float>(i-1)) ) ,
  //                      Point( bin_w*(i), hist_h - cvRound(histograms[1].at<float>(i)) ),
  //                      Scalar( 0, 255, 0), 2, 8, 0  );
  //     line( histImage, Point( bin_w*(i-1), hist_h - cvRound(histograms[2].at<float>(i-1)) ) ,
  //                      Point( bin_w*(i), hist_h - cvRound(histograms[2].at<float>(i)) ),
  //                      Scalar( 0, 0, 255), 2, 8, 0  );
  // }

  return histograms;
}

int ImageHandeler::generateAndSaveHist( string fpath ){
  Mat im = imread( fpath );
  if(!im.data)
    cout << "Error while opening image " << fpath << endl;
  vector<Mat> bgr_hist = this->getHist( im );

  Mat im_hsi = this->bgr2hsi( im );
  vector<Mat> hsi_hist = this->getHist( im_hsi );

  string endpath = fpath;
  endpath.erase( endpath.length() - 4, endpath.length() );
  endpath.erase( 1, 16 );
  endpath.insert(1, "/histograms");
  
  endpath += + "_hist_b";
  const char* path =  endpath.c_str();
  this->writeHist(path, bgr_hist[0]);

  endpath.erase( endpath.length() - 7, endpath.length() );
  endpath += + "_hist_g";
  path =  endpath.c_str();
  this->writeHist(path, bgr_hist[1]);

  endpath.erase( endpath.length() - 7, endpath.length() );
  endpath += + "_hist_r";
  path =  endpath.c_str();
  this->writeHist(path, bgr_hist[2]);

  endpath.erase( endpath.length() - 7, endpath.length() );
  endpath += + "_hist_h";
  this->writeHist(path, hsi_hist[0]);

  endpath.erase( endpath.length() - 7, endpath.length() );
  endpath += + "_hist_s";
  path =  endpath.c_str();
  this->writeHist(path, hsi_hist[1]);

  endpath.erase( endpath.length() - 7, endpath.length() );
  endpath += + "_hist_i";
  path =  endpath.c_str();
  this->writeHist(path, hsi_hist[2]);
}
