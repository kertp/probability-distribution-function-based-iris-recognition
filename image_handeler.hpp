#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>


#define BINS 256
#define UNIFORM true
#define ACCUMULATE false
#define PI 3.141592653589793

using namespace cv;
using namespace std;

struct range{
  int start;
  int end;
};

class ImageHandeler{

  struct range get_row_range( Mat , Mat );
  struct range get_col_range( Mat , Mat );
  int save_img( Mat, string );
  vector<Mat> splitImg( Mat );
  vector<Mat> getHist( Mat );
  int rad2deg( int );
  int writeHist( const char *, Mat );
  Mat genHist( Mat, int );

public:
  Mat bgr2hsi( Mat );
  int filter( string, string );
  int generateAndSaveHist( string );

};
