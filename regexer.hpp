#include <string>
#include <regex.h>
#include <vector>

using namespace std;

class Regexer{
  regex_t reg;  
  regmatch_t matches[2];

public:
  // Method for matching a string against a regex. Case insensitive
  // Returns 1 if a match exists, 0 if match doesn't exist and -1 if there was an exception while finding the match.
  int find( string, string );
};