#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "reader.hpp"
#include "image_handeler.hpp"


using namespace std;

int main(){
  Reader re;
  ImageHandeler ih;
  string path = "./images/filtered/"; 
  vector<string> result;
  result = re.match( path, ".*png" );
  for(int i = 0; i < result.size(); i++){
    cout << "Generating histogram for " << result[i] << "." << endl;
    ih.generateAndSaveHist(result[i]);
  }
  ofs.close();
}