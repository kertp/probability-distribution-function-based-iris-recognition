#include <regex.h>
#include <string>
#include <iostream>
#include "regexer.hpp"

using namespace std;

int Regexer::find( string str, string regex ){
  try{
    regcomp( &reg, regex.c_str(), REG_EXTENDED | REG_ICASE );
    // Once a match has been found regexec will return zero
    int match = regexec( &reg, str.c_str(), 2, this->matches, 0 );

    regfree( &reg );

    return !match;
  }
  catch( const char* msg ){
    cout << msg << endl;
    return -1;    
  }
}