#include <string>
#include <vector>

using namespace std;

class Reader{

  vector<string> read( string );
public:
  // Method for recursive file scanning and matching in Unix. Returns a vector of all the files that match the specified regex
  vector<string> match( string, string );
};