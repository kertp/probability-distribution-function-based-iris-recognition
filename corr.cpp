#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "correlator.hpp"
#include "reader.hpp"

using namespace std;

int main(){
  ofstream ofs;
  Reader re;
  Correlator co;
  // string file1 = "histograms/iris1_8/001L_1_hist_b";
  // string file2 = "histograms/iris1_8/002L_2_hist_b";
  // cout << co.get_kullback_leiber_divergence(file2, file1) << endl;

  ofs.open( "test_corre.csv", ofstream::out | ofstream::trunc );
  ofs << ", Cross correlation, , , , ,, KLD" << endl;
  ofs << "#, b, g, r, h, s, i, b, g, r, h, s, i" << endl;
  
  std::vector<string> chns(6);
  chns = { "b", "g", "r", "h", "s", "i" };
  string initial_training = "015L_3_hist_";
  string initial_test = "2_hist_";

  string search_regex_for_test_set;
  string search_regex_for_training_set;
  string str_to_csv;
  vector<double> maximums(6);
  vector<int> names(6);
  for(int j = 1; j < 65; j++){

    search_regex_for_test_set = "0" + to_string(j) + "L_" + initial_test;
    search_regex_for_training_set = initial_training;
    str_to_csv = to_string(j);
    double *cross_corr_vect = new double[chns.size()];
    double *klic_vect = new double[chns.size()];
    for(int i = 0; i < chns.size(); i++){
      vector<string> training_iris = re.match( "./histograms", search_regex_for_training_set + chns[i]);
      vector<string> test_iris = re.match( "./histograms", search_regex_for_test_set + chns[i]);
      cout << test_iris[0] << endl;
      cross_corr_vect[i] = co.get_cross_correlation( test_iris[0], training_iris[0] );
      klic_vect[i] = co.get_kullback_leiber_divergence( test_iris[0], training_iris[0] );
    }
    for(int i = 0; i < chns.size(); i++){
      str_to_csv += (", " + to_string(cross_corr_vect[i]));
      if(maximums[i] < cross_corr_vect[i]){
        maximums[i] = cross_corr_vect[i];
        names[i] = j;
      }
    }
    for(int i = 0; i < chns.size(); i++){
      str_to_csv += (", " + to_string(klic_vect[i]));
    }
    ofs << str_to_csv << endl;
  }
  for(int i = 0; i < chns.size(); i++){
    cout << chns[i] << ": " << maximums[i] << " for " << names[i] << endl;
  }
  ofs.close();
}