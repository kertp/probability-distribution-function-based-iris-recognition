#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <string>

#include "correlator.hpp"
using namespace std;

int* Correlator::openHist( string file ){

  ifstream hist_txt;
  hist_txt.open( file.c_str() );
  string output;

  int *histVect = new int[BINS];
  if( hist_txt.is_open() ) {

    int i = 0;
    while( !hist_txt.eof() ){

      hist_txt >> output;
      histVect[i] = atoi( output.c_str() );
      i++;

    }

  }
  return histVect;
}

double Correlator::generate_cross_correlation(int *testHist, int *trainingHist ){
  double mx = this->avg( testHist );
  double my = this->avg( trainingHist );
  double numerator = 0;
  for( int i = 0; i < BINS; i++){
    numerator += (testHist[i] - mx) * (trainingHist[i] - my);
  }
  double denominator = this->std_dev(testHist) * this->std_dev(trainingHist);
  return numerator / (denominator);
}

double Correlator::std_dev( int *array ){
  double m = this->avg( array );
  double s = 0;
  for( int i = 0; i <= BINS; i++){
    s += pow( array[i] - m, 2 );
  }
  return sqrt( s );
}

double Correlator::avg( int *array ){
  double m = 0;
  for( int i = 0; i < BINS; i++ ){
    m += array[i];
  }
  m /= BINS;
  return m;
}

double Correlator::get_cross_correlation( string file, string second_file ){
  int *trainingHist = this->openHist( file );
  int *testHist = this->openHist( second_file );
  return generate_cross_correlation( testHist, trainingHist );
}

double* Correlator::probability_density_function( int* input ){
  double *output = new double[BINS];
  double sum = 0;
  for(int i = 0; i < BINS; i++)
    sum += input[i];
  double second_sum = 0;
  for(int i = 0; i < BINS; i++){
    output[i] = input[i] /  sum;
    second_sum += output[i];
  }
  return output;
}

double Correlator::get_kullback_leiber_divergence( string test, string training ){
  int *trainingHist = this->openHist( test );
  int *testHist = this->openHist( training );

  double* normalized_training = this->probability_density_function( trainingHist );
  double* normalized_test = this->probability_density_function( testHist );

  return this->generate_kullback_leiber_divergence( normalized_training, normalized_test );
}

//http://math.stackexchange.com/questions/219462/kullback-leibler-distance-between-2-probability-distributions
double Correlator::generate_kullback_leiber_divergence( double* test, double* training ){
  double divergence = 0;
  for(int i = 0; i < BINS; i++){
    divergence += log( (test[i]+0.000001) / (training[i]+0.000001) ) * (test[i]+0.000001);
  }
  if(divergence < 0)
    cout << "Cant be real!" << endl;
  return divergence;
}