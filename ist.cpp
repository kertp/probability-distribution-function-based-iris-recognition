#include <opencv2/opencv.hpp>
#include <iostream>
#include <stdio.h>
#include <ftw.h>
#include <fnmatch.h>
#include <math.h>

// Exceptions
#include <stdexcept>

// IO
#include <iostream>
#include <fstream>

// min
#include <algorithm>

#define BINS 256
#define UNIFORM true
#define ACCUMULATE false

using namespace std;
using namespace cv;

int callback( const char *, const struct stat *, int );
void getHist( Mat );
vector<Mat> splitImg ( Mat );
Mat genHist( Mat, int );
Mat bgr2hsi( Mat );

int main(){
  // ftw( "./images/filtered", callback, FTW_F );
  Mat im = imread( "Lenna.png" );
  if(!im.data)
    cout << "Error while opening image " << fpath << endl;
  return 0;
}

int callback(const char *fpath, const struct stat *sb, int typeflag) {
    if (typeflag == FTW_F) {
      if (fnmatch("*.png", fpath, FNM_CASEFOLD) == 0) {

        Mat im = imread( fpath );
        if(!im.data)
          cout << "Error while opening image " << fpath << endl;
        // getHist(im);
        Mat hsi = bgr2hsi( im );
        // getHist(hsi);
      }
    }
    return 0;
}

void getHist( Mat input ){
  // cvtColor(iltered,input,CV_BGR2HSV, 0);
  vector<Mat> channels(3);
  channels = splitImg( input );

  vector<Mat> histograms(3);
  for(int i = 0; i < 3; i++){
    histograms[i] = genHist( channels[i], BINS);
  }
    
  int hist_w = 800; int hist_h = 800;
  int bin_w = cvRound( (double) hist_w/BINS );

  Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );
  
  histograms[0].at<float>(0) = 0;
  histograms[1].at<float>(0) = 0;
  histograms[2].at<float>(0) = 0;


  normalize(histograms[0], histograms[0], 0, histImage.rows, NORM_MINMAX, -1, Mat() );
  normalize(histograms[1], histograms[1], 0, histImage.rows, NORM_MINMAX, -1, Mat() );
  normalize(histograms[2], histograms[2], 0, histImage.rows, NORM_MINMAX, -1, Mat() );

  // / Draw for each channel
  for( int i = 1; i < BINS; i++ )
  {
      line( histImage, Point( bin_w*(i-1), hist_h - cvRound((histograms[0]).at<float>(i-1)) ) ,
                       Point( bin_w*(i), hist_h - cvRound((histograms[0]).at<float>(i)) ),
                       Scalar( 255, 0, 0), 2, 8, 0  );
      line( histImage, Point( bin_w*(i-1), hist_h - cvRound(histograms[1].at<float>(i-1)) ) ,
                       Point( bin_w*(i), hist_h - cvRound(histograms[1].at<float>(i)) ),
                       Scalar( 0, 255, 0), 2, 8, 0  );
      line( histImage, Point( bin_w*(i-1), hist_h - cvRound(histograms[2].at<float>(i-1)) ) ,
                       Point( bin_w*(i), hist_h - cvRound(histograms[2].at<float>(i)) ),
                       Scalar( 0, 0, 255), 2, 8, 0  );
  }

  namedWindow("Iris", CV_WINDOW_AUTOSIZE);
  imshow("Iris", input);
  
  namedWindow("Hist", CV_WINDOW_AUTOSIZE);
  imshow("Hist", histImage);
  waitKey(0);
}

vector<Mat> splitImg( Mat input ){

  vector<Mat> bgr_planes(3);
  split( input, bgr_planes );
  return bgr_planes;
}

Mat genHist( Mat image, int bins ){

  if(image.channels() != 1){
    stringstream sstm;
    sstm << "The image has " << image.channels() << " channels instead of one. Please split it!" << endl;
    string result = sstm.str();
    throw std::invalid_argument( result );
  }

  // defining some variables
  Mat hist;
  float range[] = { 0, 256 } ;
  const float* histRange = { range };
  // http://docs.opencv.org/modules/imgproc/doc/histograms.html?highlight=calchist#void calcHist(const Mat* images, int nimages, const int* channels, InputArray mask, SparseMat& hist, int dims, const int* histSize, const float** ranges, bool uniform, bool accumulate)
  calcHist( &image, 1, 0, Mat(), hist, 1, &bins, &histRange, UNIFORM, ACCUMULATE );
  return hist;
}

Mat bgr2hsi( Mat image ){
  Mat hsi;
  int a;
  hsi.create(image.rows, image.cols, CV_8UC3);
  for(int i = 0; i < image.rows; i++){
    // cout << i << endl;
    for(int j = 0; j < image.cols; j++){
      float hue, saturation, intensity, minimum;
      float b = image.at<Vec3b>(i,j)[0];
      float g = image.at<Vec3b>(i,j)[1];
      float r = image.at<Vec3b>(i,j)[2];


      intensity = (b + g + r) / 3;
      minimum = min( b, min( g, r ) );
      

      if( intensity == 0 ){
        saturation = 0;
      }
      else{
        saturation = 1 - minimum/intensity;
      }

      if(g >= b){
        hue = acos((r - 0.5*g - 0.5*b)/(sqrt(pow( r, 2 ) + pow( g, 2 ) + pow( b, 2 ) - r*g - r*b - g*b)));
      }
      else{
        hue = 360 - acos((r - 0.5*g - 0.5*b)/(sqrt(pow( r, 2 ) + pow( g, 2 ) + pow( b, 2 ) - r*g - r*b - g*b))); 
      }


      cout << "R " << r << endl;
      cout << "G " << g << endl;
      cout << "B " << b << endl;
      cout << "H " << hue << endl;
      cout << "S " << saturation << endl;
      cout << "I " << intensity << endl;

      hsi.at<Vec3b>(i,j)[0] = hue;
      hsi.at<Vec3b>(i,j)[1] = saturation;
      hsi.at<Vec3b>(i,j)[2] = intensity;
    } 
  }
  return hsi;
}
