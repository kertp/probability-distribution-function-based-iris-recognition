#include <iostream>
#include <string>

#define BINS 256

using namespace std;

class Correlator{

  int* openHist( string );
  double std_dev( int * );
  double avg( int * );
  double generate_cross_correlation(int*, int*);

  double generate_kullback_leiber_divergence( double*, double* );
  double* probability_density_function( int* );

public:

  double get_cross_correlation( string, string );
  double get_kullback_leiber_divergence( string, string );
};